#!/bin/bash

update_file_synonym () {

    csvgrep -c 1 -m "$accepted_name" $1 | csvcut -c 2 | csvtool readable - |sed 1d | sed "s/\(.*\)/> \"$species\",\"\1\"/"
    csvgrep -c 1 -m "$accepted_name" $1 | csvcut -c 2 | csvtool readable - |sed 1d | sed "s/\(.*\)/\"$species\",\"\1\"/" >> $1
}

get_uniq_options () {
    
    lines=`csvcut -c 2 $1 | sort | uniq | wc -l`

    if [ $lines -gt 20 ]; then
        csvcut -c 2 $1 | sort | uniq | head -n 20
    else
        csvcut -c 2 $1 | sort | uniq
    fi
}

update_file_new () {
    
    echo -e "\nDo you want to see the text options in $1?"
    select yn in "Yes" "No"; do
        case $yn in
            Yes ) 
                get_uniq_options $1
                break;;
            No ) break;;
        esac
    done

    add_line $1 $species
}

update_file () {
    
    echo -e "\nDo you want to see the text options in $1?"
    select yn in "Yes" "No"; do
        case $yn in
            Yes ) 
                get_uniq_options $1
                break;;
            No ) break;;
        esac
    done

    echo -e "\nWhat would you like to do Add, Update or Delete line?";
    select ud in "Update" "Delete" "Add"; do
        case $ud in
            Update ) 

                add_line $1 $species

                break;;
            Delete ) 
                echo -e "\nThis line would you like to delete?"
                grep -n "$species" $1
                
                select yn in "Yes" "No"; do
                    case $yn in
                        Yes ) 
                            line=`grep -n "$species" $1  | awk -F : '{print $1}'`
                            delete_line $line $1

                            break;;
                        No ) 
                            echo -e "Give the line number please"
                            read;

                            delete_line ${REPLY} $1
                            break;;
                    esac
                done

                break;;
            Add )
                add_line $1 $species
                break;;
        esac
    done

}

add_line () {
    echo -e "\nWhat text would like to add in $1?"

    read;
    echo '> "'$species'","'${REPLY}'"'
    echo '"'$species'","'${REPLY}'"' >> $1

}

delete_line () {
    #sed -i '/pattern to match/d' ./infile
    sed -e ''$1'd' $2 > $2_tmp
    echo -e "\nIs it good result to perform delete operation?"
    diff $2 $2_tmp
    select yn in "Yes" "No"; do
        case $yn in
            Yes ) 
                mv $2_tmp $2
                break;;
            No ) 
                rm $2_tmp
                break;;
        esac
    done

}

s=$'\e[0;32m' 
e=$'\e[m'
PS3="${s}Type the number :${e} "

echo "
Git commands:
   git clone https://gitlab.com/openbiomaps/superspecies.git 
 OR 
   git pull
git checkout -b new_updates

cd superspecies/data"

echo -e "\nWhat would you like to do?"

select opt in "Add a new species?" "Add a new synonym?" "Update existing species?" "Search for names" "None of either above"; do

  case $opt in
    "Add a new species?")
      read -p "New species name?: " species
      action=new_species
      break
      ;;
    "Add a new synonym?")
      read -p "New species (synonym) name?: " species
      read -p "Existing species (accepted) name?: " accepted_name
      action=new_synonym
      break
      ;;
    "Update existing species?")
      read -p "Existing species name?: " species
      action=update_species
      break
      ;;
    "Search for names")
      read -p "Type peace of name: " species
        
      grep --color=always "$species" ssp_speciesnames.csv

      echo -e "\nDo you want search for other files?"
      select yn in "Yes" "No"; do
            case $yn in
                Yes ) 
                    ls ssp_*.csv | grep -v ssp_speciesnames | xargs grep --color=always "$species"
                    break;;
                No ) break;;
            esac
      done

      echo -e "\nDo you want search for $species in GBiF?"
      select yn in "Yes" "No"; do
            case $yn in
                Yes ) 
                    f=`echo $species | sed -e "s/ /%20/"`
                    mkdir /tmp/gbif_api_use_$f/
                    # Get whole results object, and jq parsed for users if they wanna to search in..
                    curl "https://api.gbif.org/v1/species?name=$f" | jq > /tmp/gbif_api_use_$f/results.json

                    cat  /tmp/gbif_api_use_$f/results.json | jq '.results[].key' > /tmp/gbif_api_use_$f/key.csv
                    cat  /tmp/gbif_api_use_$f/results.json | jq '.results[].kingdom' > /tmp/gbif_api_use_$f/kingdom.csv
                    cat  /tmp/gbif_api_use_$f/results.json | jq '.results[].parent' > /tmp/gbif_api_use_$f/parent.csv
                    cat  /tmp/gbif_api_use_$f/results.json | jq '.results[].scientificName' > /tmp/gbif_api_use_$f/scientificName.csv
                    cat  /tmp/gbif_api_use_$f/results.json | jq '.results[].taxonomicStatus' > /tmp/gbif_api_use_$f/taxonomicStatus.csv
                    cat  /tmp/gbif_api_use_$f/results.json | jq '.results[].accepted' > /tmp/gbif_api_use_$f/accepted.csv


                    csvtool paste /tmp/gbif_api_use_$f/key.csv /tmp/gbif_api_use_$f/kingdom.csv /tmp/gbif_api_use_$f/parent.csv /tmp/gbif_api_use_$f/scientificName.csv /tmp/gbif_api_use_$f/taxonomicStatus.csv /tmp/gbif_api_use_$f/accepted.csv | csvtool readable -

                    #echo -e "\nSee whole results in /tmp/gbif_api_use_$f/results.json"
                    echo -e "\nWould you like to see distribution data of specific record?\nType its key"
                    read;

                    curl "https://api.gbif.org/v1/species/${REPLY}/distributions" | jq | cat -

                    # Using CoL API
                    # curl -X 'GET' 'https://api.catalogueoflife.org/nameusage/search?content=SCIENTIFIC_NAME&sortBy=RELEVANCE&q='$f'&minRank=SUBSPECIES&maxRank=SPECIES_AGGREGATE&type=WHOLE_WORDS&offset=0&limit=100' -H 'accept: application/json' | jq '.result[].usage.label, .result[].usage.status'
                    break;;
                No ) break;;
            esac
      done

      exit
      ;;
    "None of either above")
      exit
      ;;
    *) 
      echo "Invalid option $REPLY"
      ;;
  esac
done


if [[ "$action" == "new_species" ]]; then

    c=`grep -o "\"$species\"" ssp_speciesnames.csv | wc -l`

    if [ $c -ne 0 ]; then
    
        echo -e "\n$species already exists!\n"
        grep --color=always "$species" ssp_*.csv

        exit
    fi

    grep --color=always "$species" ssp_*.csv

    echo -e "\nAdd '$species' to ssp_speciesnames.csv?"
    select yn in "Yes" "No"; do
        case $yn in
            Yes ) 
                echo '"'$species'"' >> ssp_speciesnames.csv
                tail ssp_speciesnames.csv
                break;;
            No ) break;;
        esac
    done
    
    # question for all other columns
    echo -e "\nAdd additional information to other columns?"
    
    select yn in "Yes" "No"; do
        case $yn in
            Yes )
                break
                ;;
            No ) 
                break
                ;;
        esac
    done
    if [[ "$yn" == "Yes" ]]; then
        select file in `ls ssp_*.csv` quit; do
            case $file in
                quit ) 
                    break
                    ;;
                * ) 
                    echo "Update $file"
                    update_file_new $file
                    ;;
            esac
        done
    fi

fi



# 2)

if [[ "$action" == "new_synonym" ]]; then
    grep --color=always "$species" ssp_*.csv

    echo -e "\nAdd '$species' to ssp_speciesnames.csv?"
    select yn in "Yes" "No"; do
        case $yn in
            Yes ) 
                echo '"'$species'"' >> ssp_speciesnames.csv
                tail ssp_speciesnames.csv
                break;;
            No ) break;;
        esac
    done

    echo -e "\nAdd '$species' to ssp_namestatus.csv?"
    select yn in "Yes" "No"; do
        case $yn in
            Yes ) 
                echo '"'$species'","synonym for '$accepted_name'"' >> ssp_namestatus.csv
                tail ssp_namestatus.csv
                break;;
            No ) break;;
        esac
    done


    # Add all other columns' content with the new name
    # 

    grep "$accepted_name" ssp_*.csv | awk -F : '{print $1}' | uniq | sed '/ssp_speciesnames.csv/d'

    echo -e "\nDo you want add the values of $accepted_name with the $species in some table?"

    # questions about files
    if [[ "$yn" == "Yes" ]]; then
        select file in `grep "$accepted_name" ssp_*.csv | awk -F : '{print $1}' | uniq | sed '/ssp_speciesnames.csv/d'` quit; do
            case $file in
                quit ) 
                    break
                    ;;
                * ) 
                    echo "Update $file"
                    update_file_synonym $file
                    ;;
            esac
        done
    fi

fi


if [[ "$action" == "update_species" ]]; then
   echo -e "\nWhich file would you like to update?"

    # questions about each files
    select file in `ls ssp_*.csv` quit; do
        case $file in
            quit ) 
                break
                ;;
            * ) 
                echo "Update $file"
                update_file $file
                ;;
        esac
    done


fi


echo '
git add .
git commit -m "New synonym added for Abax parallelepipedus"
git push

my_private_token=xxx
curl --header "PRIVATE-TOKEN: $my_private_token" --request POST -d "source_branch=new_updates" -d "target_branch=master" -d "title=updates" "https://gitlab.com/api/v4/projects/21101548/merge_requests"
'
