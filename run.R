
# Fetching superspecies data/*.csv files from gitlab
fetch <- function(branch) {
    library('httr')
    library('RCurl')

    dir.create('data', showWarnings = FALSE)
    setwd('data')

    repository_id <- 36471418

    req <- GET(paste0("https://gitlab.com/api/v4/projects/",repository_id,"/repository/tree?ref=",branch,"&path=data&per_page=100"))
    x <- content(req,"parsed")
    i<-1
    for (i in seq(i,length(x))) { 
        file <- x[[i]]$name
        print(file)
        req <- GET(paste0("https://gitlab.com/api/v4/projects/",repository_id,"/repository/files/data%2F",file,"?ref=",branch))
        v<-content(req,"parsed")
        if (v$size>0) {
            write(base64Decode(v$content),file=file)
        }
    }

    file <- 'ssp_config.json'
    print(file)
    req <- GET(paste0("https://gitlab.com/api/v4/projects/",repository_id,"/repository/files/",file,"?ref=",branch))
    v<-content(req,"parsed")
    write(base64Decode(v$content),file=file)
    
    setwd('../')
}

# GitLab API client
git <- function(file=NULL,content=NULL,branch=NULL,author_email=NULL,author_name=NULL,commit_message=NULL) {
    #library('git2r')
    #git2r::add(repo = ".", path = NULL, force = FALSE)
    #git2r::commit(repo = ".", message = "shiny app updates")
    #git2r::push(credentials = 'whatever-you-need-for-pushing-to-bitbucket') 

    source('git_config.R')
    if (is.null(content)) {
        return("Add some content")
    }
    if (is.null(file)) {
        return("A file name is obligatory")
    }

    if (!is.null(branch)) {
        commit_config$branch <- branch
    }
    if (!is.null(author_name)) {
        commit_config$author_name <- author_name
    }
    if (!is.null(author_email)) {
        commit_config$author_email <- author_email
    }
    if (!is.null(commit_message)) {
        commit_config$commit_message <- commit_message
    }
    commit_config$content <- content

    # The base url for gitlab OpenBioMaps/Superspecies data folder 
    base_url <- paste0(base_url,"%2F",file)
    
    # headers
    headers <- httr::add_headers(.headers = c(
        "PRIVATE-TOKEN" = access_token,
        "Content-Type"="application/json")
    )
    
    # move everything else to the body. grant_type and password were requested by the endpoint
    # nem lehet a csv fájlt helyesen így betenni...
    body <- rjson::toJSON(commit_config)

    new_file <- FALSE
    # post/put call to get the token
    if (new_file) {
        # new file
        h <- httr::POST(
          url  = base_url,
          body = body,
          config  = headers,
          httr::accept_json()
        )
    } else {
        # update
        h <- httr::PUT(
          url  = base_url,
          body = body,
          config  = headers,
          httr::accept_json()
        )
    }

    # Response    
    if (httr::status_code(h) != 200) {
        return(paste("http error:",httr::status_code(h),h ))
    }
    j<-httr::content(h, "parsed", "application/json")
    j
} 

if (file.exists('data/ssp_config.json')) {
    source('shinyapp.R')
    #cat ("Run:\nshinyApp(ui = ui, server = server)\n")
} else {
    source('git_config.R')
    cat ("Fetch data files:\nfetch(",commit_config$branch,")\n")
}
