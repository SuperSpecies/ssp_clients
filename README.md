# ssp_clients

SuperSpecies client applications and other related tools



## Updating the database - adding new rows into some atomic tables

### R Shiny App

R
```
>
> source('run.R')
> fetch()
```
You may need to install some packages like sqldf, shinyjs, taxize

R
```
> install.packages('sqldf')
```
Once this has been done

Visit the http://127.0.0.1:3461/

### linux_cml_client.sh

Examples:

Adding a new species names

Check the new name in the database 
```
grep "Spinus spinus" ssp_*.csv
```

and check the synonyms
```
grep "Carduelis spinus" ssp_*.csv
```

Get last index of speciesnames:
```
tail -n -1 ssp_speciesnames.csv
```
Add the new name to the database
```
echo "50633","Spinus spinus" >> ssp_speciesnames.csv
```
Add to other tables:
```
echo '"Spinus spinus","25000"' >> ssp_eszmeiertek.csv
echo '"Spinus spinus","FV"' >> ssp_iagreements_bern.csv
echo '"Spinus spinus","csíz"' >> ssp_nationalnames_hun.csv
echo '"Spinus spinus","BD_2"' >> ssp_natura2000.csv
echo '"Spinus spinus","CARSPI"' >> ssp_shortnames.csv
echo '"Spinus spinus","Védett"' >> ssp_vedettseg.csv
echo '"Spinus spinus","madarak"' >> ssp_taxon_hun.csv
```

Update a table
```
echo '"Spinus spinus","accepted name"' >> ssp_namestatus.csv
```
update Carduelis spinus to synonym.... ?? in libreoffice or using csvsql


USING csvkit in-memory SQL database
```
csvsql -i postgresql ssp_namestatus.csv
csvsql --query "UPDATE ssp_namestatus SET namestatus='synonym for Spinus spinus' WHERE species_name='Carduelis spinus'; SELECT * FROM ssp_namestatus" ssp_namestatus.csv > ssp_namestatus.csv
```

